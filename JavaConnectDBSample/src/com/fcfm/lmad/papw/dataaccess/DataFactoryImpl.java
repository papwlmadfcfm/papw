/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.dataaccess;

import com.fcfm.lmad.papw.Main;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yo_36
 */
public class DataFactoryImpl implements IDataFactory {
    
    private boolean execute;
    
    public boolean execute() {
        return this.execute;
    }            
    
    public void executeMoreResults(boolean res) {
        this.execute = res;
    }
    
    
    @Override
    public Connection getDbConnection() {        
        try {
            try {
                // Load Connector J Driver
                Class.forName("com.mysql.cj.jdbc.Driver");
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }
            
            // Establish connection
            return DriverManager.getConnection("jdbc:" + dbms + "://" + host + ":" + port + "/" + db + "?useSSL=false&serverTimezone=UTC"
                    , user, pswd);
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public CallableStatement executeSimpleStoredProcedure(Connection argConnection, 
            String argSpName, 
            List<String> argSpParams, 
            List<Class<?>> argSpTypes) {
        try {
            CallableStatement stmt = argConnection.prepareCall(argSpName);            
            if (stmt != null) {
                int index = 1;
                
                if (Integer.compare(argSpParams.size(), argSpTypes.size()) != 0) {
                    return null;
                }
                for (int i = 0; i < argSpParams.size(); i++) {
                    if (argSpTypes.get(i) == String.class) {
                        stmt.setString(index++, argSpParams.get(i));
                    } else if (argSpTypes.get(i) == int.class) {
                        stmt.setInt(index++, Integer.valueOf(argSpParams.get(i)));
                    } else if (argSpTypes.get(i) == boolean.class) {
                        stmt.setBoolean(index++, Boolean.valueOf(argSpParams.get(i)));
                    } else if (argSpTypes.get(i) == Date.class) {
                        stmt.setDate(index++, Date.valueOf(argSpParams.get(i)));
                    } else if (argSpTypes.get(i) == Long.class) {
                        stmt.setLong(index++, Long.valueOf(argSpParams.get(i)));
                    }
                }
                
                execute = stmt.execute();
                if (execute) {
                    return stmt;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataFactoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
