/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.dataaccess;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author yo_36
 */
public class DataFactory {
    
    private static IDataFactory impl_ = null;

    public static Connection getDbConnection() {
        return getImpl().getDbConnection();
    }
    
    public static CallableStatement executeSimpleStoredProcedure(Connection argConnection, String argSpName, List<String> argSpParams, List<Class<?>> argSpTypes) {
        return getImpl().executeSimpleStoredProcedure(argConnection, argSpName, argSpParams, argSpTypes);
    }
    
    public static boolean execute() {
        return ((DataFactoryImpl) getImpl()).execute();
    }
    
    public static void executeMoreResults(boolean res) {
        ((DataFactoryImpl) getImpl()).executeMoreResults(res);
    }
    
    private static IDataFactory getImpl() {
        if (impl_ == null) {
            impl_ = new DataFactoryImpl();
        }
        return impl_;
    }
    
    // Evitar crear objetos de la clase
    private DataFactory() {
        
    }
            
    
}
