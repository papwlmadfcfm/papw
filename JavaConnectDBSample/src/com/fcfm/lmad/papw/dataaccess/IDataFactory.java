/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.dataaccess;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author yo_36
 */
public interface IDataFactory {
    public static final String user = "root"; // colocar usuario a su BD en MySQL
    public static final String pswd = null; // colocar contraseña a su BD en MySQL
    public static final String dbms = "mysql";
    public static final String host = "127.0.0.1";
    public static final String port = "3306";
    public static final String db = "mypapwbd";
        
    Connection getDbConnection();
    
    CallableStatement executeSimpleStoredProcedure(Connection argConnection, String argSpName, List<String> argSpParams, List<Class<?>> argSpTypes);
}
