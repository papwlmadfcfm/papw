/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.factory;

import com.fcfm.lmad.papw.Main;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yo_36
 */
public class InitializeFactories {
    
    public static void init() {
        Properties props = new Properties();
        try {
            FileInputStream in = new FileInputStream("system.properties");
            props.load(in);
            in.close();
            System.setProperties(props);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
}
