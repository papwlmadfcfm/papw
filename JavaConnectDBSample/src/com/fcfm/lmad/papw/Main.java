package com.fcfm.lmad.papw;

import com.fcfm.lmad.papw.dataaccess.DataFactory;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yo_36
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /**************************/
        // problemas al cargar propiedades; no abre conexion mysql
        //InitializeFactories.init();
        /**************************/
        
        // stored procedure with parameter(s)
        final String spGetUserByUsername = "{CALL sp_get_usuario_by_username(?)}";
        final String spParamUsername = "Geraberto";

        try {
            Connection conn = DataFactory.getDbConnection();
            List<String> spParamsValues = new ArrayList<String>();
            List<Class<?>> spParamsTypes = new ArrayList<Class<?>>();
            
            // Valor de parámetros en orden
            spParamsValues.add(spParamUsername);
            //spParamsValues.add("1");
            //spParamsValues.add("0");
            
            // Tipo de parámetros en orden
            spParamsTypes.add(String.class);
            //spParamsTypes.add(int.class);
            //spParamsTypes.add(boolean.class);
            
            CallableStatement stmt = 
                    DataFactory.executeSimpleStoredProcedure(
                            conn, spGetUserByUsername, spParamsValues, spParamsTypes);

            // Create CallableStatement Object used to execute stored procedures that could have parameters.
            // CallableStatement stmt = conn.prepareCall(spGetUserByUsername);
            // stmt.setString(1, spParamUsername);

            // Execute a store procedure
            // Returns 'true' indicating existance of ResultSet Object
            // boolean exists = stmt.execute();

            while (DataFactory.execute()) {
                // Processes results
                ResultSet rs = stmt.getResultSet();
                while(rs.next()) {
                    // los parámetros tipo String de la función "getString" son los nombres de las columnas 
                    // resultado de ejecutar el SP sp_get_usuario_by_username definido en mi BD
                    String nombre = rs.getString("nombre");
                    String apeido = rs.getString("apeido");
                    String correo = rs.getString("correo");
                    String userpwd = rs.getString("password");
                    int telefono = rs.getInt("telefono");
                    Blob ppic = rs.getBlob("ppic");
                }
                DataFactory.executeMoreResults(stmt.getMoreResults());
            }

            // Close statement and connection
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
}
