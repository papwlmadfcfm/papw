$(function(){
    console.log("Funciona :)");
    $("p").click(function(){
        console.log(
            $(this).text()
        );
    });
    $("li a").hover(function(){
        $(this).css("background-color", "aliceblue");
    });
    $("li a").mouseleave(function(){
        $(this).css("background-color", "coral");
    });
    $("input").focus(function(){
        $(this).attr("type", "password");
    });
    $("input").blur(function(){
        $(this).attr("type", "radio");
    });
    $("button").on({
        dblclick : function(){
            console.log("Doble clic");
        },
        mouseenter : function(){
            console.log("Entré");
            $("p").css("font-weight", "bolder");
        },
        mouseleave : function(){
            console.log("Salí");
            $("p").css("font-weight", "normal");
        }
    });
    $("#p1").click(function(){
        console.log($("#p1").html());
        $(".p2").append(
            $("#p1").html()
        );
    });
    $("#btn1").click(function(){
        $("#p1").prepend(
            $("input[type='text']").val()
        );
    });
    $('#dialog-dom-loaded').dialog({
		modal: true,
		buttons: {
		Ok: function() {
			  $( this ).dialog( "close" );
			}
		}
	});
});
/*
var e1 = $("<i></i>").text("Crea cursivas");

var e2 = document.createElement("b");

e2.innerHTML = "Crea negritas";

$("#p1").before(e1, e2);

$("#ajax").click(function(){
    $("#respuestaAjax").load("ajax.html");
});


document.addEventListener('DOMContentLoaded', function() {
	$(function() {
		$('#dialog-dom-loaded').dialog({
		  modal: true,
		  buttons: {
		    Ok: function() {
		      $( this ).dialog( "close" );
		    }
		  }
		});
	});

    var elements = document.getElementsByClassName('req-inputs');
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity('');
            if (!e.target.validity.valid) {
                e.target.setCustomValidity('Este campo es requerido. Además, debe cumplir con los estándares.');
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity('');
        };
    }

    var inputs = document.querySelectorAll('input[list]');
	for (var i = 0; i < inputs.length; i++) {
	  inputs[i].addEventListener('change', function() {
	    var optionFound = false,
	      datalist = this.list;
	    for (var j = 0; j < datalist.options.length; j++) {
	        if (this.value == datalist.options[j].value) {
	            optionFound = true;
	            break;
	        }
	    }
	    if (optionFound) {
	      this.setCustomValidity('');
	    } else {
	      this.setCustomValidity('Seleccionar una opción válida dentro de los valores permitidos.');
	    }
	  });
	}
})


function validate() {
	console.clear();
	console.log('Se envían: ');
	var inputs = document.getElementsByTagName('input');
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].value !== undefined && inputs[i].value !== '') {
			console.log('Valor ' + i + ': ' + inputs[i].value);
		}
	}

	if (!passwordMatches()) {
		return false;
	}
	return true;
}



function passwordMatches() {
	var pwd1 = document.getElementById('pwd').value;
	var pwd2 = document.getElementById('pwdcfm').value;
	if (pwd2 != '' && pwd1 != pwd2) {
		document.getElementById('pwdcfm').value = "";
		//document.getElementById('pwdcfm').focus();
		alert('Las contraseñas no coinciden.');
		return false;
	}
	return true;
}
*/