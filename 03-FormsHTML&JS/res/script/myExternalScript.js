document.addEventListener('DOMContentLoaded', function() {
    var elements = document.getElementsByClassName('req-inputs');
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity('');
            if (!e.target.validity.valid) {
                e.target.setCustomValidity('Este campo es requerido. Además, debe cumplir con los estándares.');
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity('');
        };
    }

    var inputs = document.querySelectorAll('input[list]');
	for (var i = 0; i < inputs.length; i++) {
	  inputs[i].addEventListener('change', function() {
	    var optionFound = false,
	      datalist = this.list;
	    for (var j = 0; j < datalist.options.length; j++) {
	        if (this.value == datalist.options[j].value) {
	            optionFound = true;
	            break;
	        }
	    }
	    if (optionFound) {
	      this.setCustomValidity('');
	    } else {
	      this.setCustomValidity('Seleccionar una opción válida dentro de los valores permitidos.');
	    }
	  });
	}
})

function changeText() {
	document.getElementById('head3').innerHTML = "JAVASCRIPT";
}

function suma() {
	var p1 = document.getElementById('param1').value*1;
	var p2 = document.getElementById('param2').value*1;
	var res = p1 + p2;
	document.getElementById('resul').innerHTML = res;
}

function chgColor(x) {
	document.getElementById('head2').style.backgroundColor = x.value;
}

function validate() {
	console.clear();
	console.log('Se envían: ');
	var inputs = document.getElementsByTagName('input');
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].value !== undefined && inputs[i].value !== '') {
			console.log('Valor ' + i + ': ' + inputs[i].value);
		}
	}

	if (!passwordMatches()) {
		return false;
	}
	return true;
}

function passwordMatches() {
	var pwd1 = document.getElementById('pwd').value;
	var pwd2 = document.getElementById('pwdcfm').value;
	if (pwd2 != '' && pwd1 != pwd2) {
		document.getElementById('pwdcfm').value = "";
		//document.getElementById('pwdcfm').focus();
		alert('Las contraseñas no coinciden.');
		return false;
	}
	return true;
}
