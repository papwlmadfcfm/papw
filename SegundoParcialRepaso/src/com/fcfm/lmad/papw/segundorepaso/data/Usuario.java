/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.segundorepaso.data;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author yo_36
 */
public class Usuario extends AbstractUsuario {
    
    private Usuario() {
        // evita llamada a constructor default al ser 'private'
    }
    
    public Usuario(LocalDate argNacimiento) {
        this.nacimiento = argNacimiento;
    }

    @Override
    public String getNombreCompleto() {
        StringBuilder sb = new StringBuilder("");
        sb.append(pNombre).append(" ");
        if (sNombre != null && !sNombre.isEmpty()) {
            sb.append(sNombre).append(" ");
        }
        sb.append(pApeido).append(" ");
        if (sApeido != null && !sApeido.isEmpty()) {
            sb.append(sApeido);
        }
        return sb.toString();
    }

    @Override
    public int edad() {
        Calendar c = Calendar.getInstance();
        LocalDate currentDate = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        if ((nacimiento != null)) {
            return Period.between(nacimiento, currentDate).getYears();
        } else {
            return 0;
        }
    }
    
}
