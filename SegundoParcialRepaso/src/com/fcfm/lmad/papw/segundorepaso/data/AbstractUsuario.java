/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.segundorepaso.data;

import java.time.LocalDate;

/**
 *
 * @author yo_36
 */
public abstract class AbstractUsuario implements IUsuario {
    protected String pNombre;
    protected String pApeido;
    
    protected String sNombre;
    protected String sApeido;
    protected String email;
    protected LocalDate nacimiento;
    
    protected static String paisDeResidencia;
    
    static {
        paisDeResidencia = "MX";
    };

    public String getsNombre() {
        return sNombre;
    }

    public void setsNombre(String sNombre) {
        this.sNombre = sNombre;
    }

    public String getsApeido() {
        return sApeido;
    }

    public void setsApeido(String sApeido) {
        this.sApeido = sApeido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(LocalDate nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getpNombre() {
        return pNombre;
    }

    public void setpNombre(String pNombre) {
        this.pNombre = pNombre;
    }

    public String getpApeido() {
        return pApeido;
    }

    public void setpApeido(String pApeido) {
        this.pApeido = pApeido;
    }

    public static String getPaisDeResidencia() {
        return paisDeResidencia;
    }

    public static void setPaisDeResidencia(String paisDeResidencia) {
        AbstractUsuario.paisDeResidencia = paisDeResidencia;
    }
    
    @Override
    public String getNombreCompleto() {
        StringBuilder sb = new StringBuilder();
        sb.append(pNombre).append(pApeido);
        return sb.toString();
    }
    
}
