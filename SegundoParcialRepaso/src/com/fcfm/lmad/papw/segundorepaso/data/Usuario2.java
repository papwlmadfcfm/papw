/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.segundorepaso.data;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author yo_36
 */
public class Usuario2 extends AbstractUsuario {
    
    private Usuario2() {
        // evita llamada a constructor default al ser 'private'
    }
    
    public Usuario2(String argpApeido) {
        this.pApeido = argpApeido;
    }
    
    @Override
    public String getNombreCompleto() {
        StringBuilder sb = new StringBuilder("");
        if (pNombre != null & !pNombre.trim().isEmpty() && !pApeido.trim().isEmpty()) {
            sb.append(pApeido).append(", ").append(pNombre);
        }
        return sb.toString();
    }
    
    @Override
    public int edad() {
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.set(nacimiento.getYear(), nacimiento.getMonthValue(), nacimiento.getDayOfMonth());
        end.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH));
        long startTime = start.getTime().getTime();
        long endTime = end.getTime().getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24);
        
        return (int) diffDays;
    }
    
}
