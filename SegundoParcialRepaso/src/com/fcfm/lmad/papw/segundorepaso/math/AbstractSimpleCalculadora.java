/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.segundorepaso.math;

/**
 *
 * @author yo_36
 */
public abstract class AbstractSimpleCalculadora implements ISimpleCalculadora {
    public static boolean printOutResult;
    
    static {
        printOutResult = true;
    }
        
    @Override
    public int suma(int o1, int o2) {
        if (printOutResult) {
            System.out.println("El resultado de la suma es: " + String.valueOf(o1+o2));
        }
        return o1+o2;
    }
    
    @Override
    public int resta(int m, int s) {
        if (printOutResult) {
            System.out.println("El resultado de la resta es: " + String.valueOf(m-s));
        }
        return m-s;
    }            
 }
