/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.segundorepaso.math;

/**
 *
 * @author yo_36
 */
public interface ISimpleCalculadora {    
    int suma(int o1, int o2);
    
    int resta(int m, int s);
    
    int multiply(int f1, int f2);
    
    int divide(int dividendo, int divisor);
}
