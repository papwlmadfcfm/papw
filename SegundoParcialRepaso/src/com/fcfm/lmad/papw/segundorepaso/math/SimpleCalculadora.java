/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.segundorepaso.math;

/**
 *
 * @author yo_36
 */
public class SimpleCalculadora extends AbstractSimpleCalculadora {
    
    @Override
    public int multiply(int f1, int f2) {
        if (printOutResult) {
            System.out.println("El resultado de la multiplicación es: " + String.valueOf(f1*f2));
        }
        return f1*f2;
    }

    @Override
    public int divide(int dividendo, int divisor) {
        if (printOutResult) {
            System.out.println("El resultado de la división es: " + String.valueOf((int) dividendo/divisor));
        }
        return dividendo/divisor;
    }
    
}
