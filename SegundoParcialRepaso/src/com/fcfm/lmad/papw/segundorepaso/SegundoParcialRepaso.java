/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcfm.lmad.papw.segundorepaso;

import com.fcfm.lmad.papw.segundorepaso.data.IUsuario;
import com.fcfm.lmad.papw.segundorepaso.data.Usuario;
import com.fcfm.lmad.papw.segundorepaso.data.Usuario2;
import com.fcfm.lmad.papw.segundorepaso.math.AbstractSimpleCalculadora;
import com.fcfm.lmad.papw.segundorepaso.math.ISimpleCalculadora;
import com.fcfm.lmad.papw.segundorepaso.math.SimpleCalculadora;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.Month;
import java.util.Scanner;

/**
 *
 * @author yo_36
 */
public class SegundoParcialRepaso {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        LocalDate fecha = LocalDate.of(1992, Month.FEBRUARY, 29);
        IUsuario usuario = new Usuario(fecha);
        String pr1 = Usuario.getPaisDeResidencia();
        IUsuario usuario2 = new Usuario2("Pérez");
        IUsuario usuario3 = new Usuario2("Rodríguez");
        Usuario2.setPaisDeResidencia("US");
        pr1 = Usuario.getPaisDeResidencia();
        
        if (usuario instanceof Usuario) {
            Usuario u = ((Usuario) usuario);
            u.setpNombre("Juanito");
            u.setpApeido("Sánchez");
            u.setsApeido("Lara");
            int edad = u.edad();
            String nombre = u.getNombreCompleto();
            int a = 0; // misc variable para debugger
        } else if (usuario instanceof Usuario2) {
            
        }       
        
        if (usuario2 instanceof Usuario) {
            
        } else if (usuario2 instanceof Usuario2) {
            Usuario2 u = ((Usuario2) usuario2);
            u.setpNombre("Gerardo");
            u.setNacimiento(fecha);
            int edad = u.edad();
            String nombre = u.getNombreCompleto();
            int a = 0; // misc variable para debugger
        }
        
        PrintStream cout = System.out;
        cout.println("El país de residencia de los usuarios es: " + Usuario.getPaisDeResidencia());
        
        ISimpleCalculadora calc = new SimpleCalculadora();
        int suma = calc.suma(5, 7);
        int mult = calc.multiply(5, 7);
        
        SimpleCalculadora.printOutResult = false;
        ISimpleCalculadora calc2 = new SimpleCalculadora();
        int resta = calc2.resta(7, 5);
        int div = calc2.divide(70, 7);
    }
    
}
