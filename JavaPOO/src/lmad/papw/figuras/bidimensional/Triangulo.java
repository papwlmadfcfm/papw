/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lmad.papw.figuras.bidimensional;

import lmad.papw.figuras.Figura;

/**
 *
 * @author yo_36
 */
public class Triangulo extends Figura {
    
    public Triangulo() {
        
    }
    
    public Triangulo(double base, double alto) {
        this.lado = base;
        this.alto = alto;
    }

    @Override
    public double area() {
        return (lado * alto) / 2;
    }
    
    @Override
    public double volumen() {
        System.out.println("*** No se puede obtener volumen de Triangulo ***");
        return -1;
    }
}
