/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lmad.papw.figuras.bidimensional;

import lmad.papw.figuras.Figura;

/**
 *
 * @author yo_36
 */
public class Cuadrado extends Figura {
    
    public Cuadrado() {
        
    }
    
    public Cuadrado(double lado) {
        this.lado = lado;
    }

    @Override
    public double area() {
        return (this.lado * this.lado);
    }
    
    public double areaRectangular(double lado, double alto) {
        return (lado * lado);
    }
    
    @Override
    public double volumen() {
        System.out.println("*** No se puede obtener volumen de Cuadrado ***");
        return -1;
    }
}
