package lmad.papw.figuras.bidimensional;

import lmad.papw.figuras.Figura;

public class Circulo extends Figura {
    
    public Circulo() {	}
    
    public Circulo(int radio) {
        this.radio = radio;
    }

    @Override
    public double area() {
        return PI * (radio * radio);
    }
    
    @Override
    public double volumen() {
        System.out.println("*** No se puede obtener volumen de Circulo ***");
        return -1;
    }    
}
