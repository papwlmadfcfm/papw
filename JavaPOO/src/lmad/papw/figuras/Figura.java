/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lmad.papw.figuras;

/**
 *
 * @author yo_36
 */
public class Figura implements IFigura, IFiguraTridimensional {
    public static double PI;    
    public static int contadorInstancias;
    
    private int variableDinamica = 0;
    
    protected double lado;
    protected double ancho;
    protected double alto;
    
    protected double radio;
    
    static {
        PI = Math.PI;
        contadorInstancias = 0;
    }
    
    public Figura() {
        contadorInstancias++;
        variableDinamica++;
        System.out.println("Instancia creada de tipo: " + this);
        System.out.println("Static: " + String.valueOf(contadorInstancias) + " Non-static: " + String.valueOf(variableDinamica));
    }
    
    public void setOtro(int argValue) {
        variableDinamica = argValue;
    }
    
    public int getOtro() {
        return variableDinamica;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getAlto() {
        return alto;
    }

    public void setAlto(double alto) {
        this.alto = alto;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    @Override
    public double area() {
        System.out.println("Generico");
        return -1;
    }

    @Override
    public double volumen() {
        System.out.println("Generico");
        return -1;
    }
    
    public static int algo() {
        System.out.println("Llamada al metodo estatico...");
        return 0;
    }
}
