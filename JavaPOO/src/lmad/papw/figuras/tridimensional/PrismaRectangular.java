/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lmad.papw.figuras.tridimensional;

/**
 *
 * @author yo_36
 */
public class PrismaRectangular extends Cubo {
    
    public PrismaRectangular() {
        super();
    }
    
    public PrismaRectangular(double lado, double ancho, double alto) {
        super(lado);
        this.ancho = ancho;
        this.alto = alto;
    }

    @Override
    public double area() {
        return 2 * ((ancho * lado) + (alto * lado) + (alto * ancho));
    }
    
    @Override
    public double areaRectangular(double lado, double alto) {
        return lado * alto;
    }

    @Override
    public double volumen() {
        return lado * ancho * alto;
    } 
}
