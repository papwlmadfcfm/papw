/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lmad.papw.figuras.tridimensional;

import lmad.papw.figuras.Figura;

/**
 *
 * @author yo_36
 */
public class Cubo extends Figura {
    
    public Cubo() {
        
    }
    
    public Cubo(double lado) {
        this.lado = lado;
    }

    @Override
    public double area() {
        return 6 * (lado * lado);
    }
    
    public double areaRectangular(double lado, double alto) {
        return (lado * lado);
    }

    @Override
    public double volumen() {
        return (lado * lado * lado);
    }
    
}
