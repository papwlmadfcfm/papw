/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lmad.papw.figuras.tridimensional;

import lmad.papw.figuras.Figura;

/**
 *
 * @author yo_36
 */
public class Cono extends Figura {
    
    public Cono() {
        
    }
    
    public Cono(double radio, double alto) {
        this.radio = radio;
        this.alto = alto;
    }

    @Override
    public double area() {
        return PI * radio * (radio + Math.sqrt(Math.pow(alto, 2) + Math.pow(radio, 2)));
    }

    @Override
    public double volumen() {
        return (PI * Math.pow(radio, 2) * alto) / 3;
    }
    
}
