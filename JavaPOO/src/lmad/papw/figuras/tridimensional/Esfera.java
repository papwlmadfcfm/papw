/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lmad.papw.figuras.tridimensional;

import lmad.papw.figuras.Figura;

/**
 *
 * @author yo_36
 */
public class Esfera extends Figura {
    
    public Esfera() {
        
    }
    
    public Esfera(double radio) {
        this.radio = radio;
    }

    @Override
    public double area() {
        return 4 * PI * Math.pow(radio, 2);
    }

    @Override
    public double volumen() {
        return (4 * PI) / 3 * (Math.pow(radio, 3));
    }
    
}
