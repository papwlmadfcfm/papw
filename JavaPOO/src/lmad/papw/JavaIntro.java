package lmad.papw;

import com.mysql.cj.jdbc.MysqlDataSource;
import com.mysql.cj.protocol.Resultset;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import lmad.papw.figuras.Figura;

import lmad.papw.figuras.bidimensional.Cuadrado;
import lmad.papw.figuras.tridimensional.Cubo;
import lmad.papw.figuras.tridimensional.PrismaRectangular;

public class JavaIntro {
    public static void main(String[] args) {
        /*
        // WELCOME TO JAVA
        // https://www.hackerrank.com/challenges/welcome-to-java/problem 
        //1. imprimir en consola
        System.out.println("Hola Mundo!");
        System.out.println();

        // JAVA STDIN AND STDOUT PART1
        // https://www.hackerrank.com/challenges/java-stdin-and-stdout-1/problem 
        //2. solicitar entrada
        Scanner scanner = new Scanner(System.in);		
        System.out.println("Introduce una cadena: ");
        String enterStr1 = scanner.next();
        System.out.println("Capturaste: " + enterStr1);
        System.out.println("Captura un numero: ");
        int enterInt1 = scanner.nextInt();
        System.out.println("El numero es: " + enterInt1);
        System.out.println();

        // JAVA IF-ELSE
        // https://www.hackerrank.com/challenges/java-if-else/problem 
        //3. if-else
        if (!(enterInt1 % 2 == 0)) {
                System.out.println("el numero es RARO e impar");
        } else {
                if (2 <= enterInt1 && enterInt1 <= 5) {
                        System.out.println("el numero NO ES RARO");
                } else if (6 <= enterInt1 && enterInt1 <= 20) {
                        System.out.println("el numero es RARO");
                } else if (enterInt1 > 20) {
                        System.out.println("el numero NO ES RARO pero fuera de rango");
                }
        }
        System.out.println();

        // JAVA STDIN STDOUT PART2
        // https://www.hackerrank.com/challenges/java-stdin-stdout/problem 
        //4. Más entradas y salidas
        System.out.println("Necesito un entero: ");
        int enterInt2 = scanner.nextInt();
        System.out.println("Ahora un double: ");
        double enterDouble1 = scanner.nextDouble();
        System.out.println("Por ultimo, una cadena: ");
        String enterStr2 = scanner.next();
        System.out.println("Cadena: " + enterStr2);
        System.out.println("Double: " + String.valueOf(enterDouble1));
        System.out.println("Int: " + String.valueOf(enterInt2));
        System.out.println();

        // JAVA OUTPUT FORMATTING
        // https://www.hackerrank.com/challenges/java-output-formatting/problem
        //5. Imprimir cadenas con formato
        System.out.println("Captura una cadena seguido de un entero: ");
        System.out.println("====================");
        for (int i = 0; i < 3; i++) {
            String s1 = scanner.next();
            int n = scanner.nextInt();
            System.out.printf("%-15s%03d%n", s1, n);
        }
        System.out.println("====================");
        System.out.println();

        // JAVA LOOPS 1
        // https://www.hackerrank.com/challenges/java-loops-i/problem 
        //6. Ciclos
        System.out.println("Ahora, un entero del 1 al 99 para dar sus multiplos");
        int enterInt3 = scanner.nextInt();
        System.out.println("¿Cuantos multiplos quieres?");
        int enterInt4 = scanner.nextInt();
        int[] multiplos = new int[enterInt4];
        for (int i = 1; i <= enterInt4; i++) {
                multiplos[i-1] = enterInt3*i;
                System.out.println(String.valueOf(enterInt3) + " * " + String.valueOf(i) + " = " + String.valueOf(multiplos[i-1]));
        }
        System.out.print("Enlistados son: ");
        List<Integer> enlistados = new ArrayList<Integer>(multiplos.length);
        for (int multiplo: multiplos) {
                enlistados.add(multiplo);
                System.out.print(String.valueOf(multiplo) + " ");
        }
        enlistados.add(enterInt3 * (enterInt4 + 1));
        System.out.println("El extra es: " + enlistados.get(enlistados.size() - 1));
        System.out.println();
        scanner.close();
        */
          
        PrintStream ps = System.out;
        Scanner scanData = new Scanner(System.in);
        double datoCapturado;
        // CLASES
        Figura cubito, prismaRect, circulo, esfera, triangulo, cono;
        Figura.algo();
        
        Figura cuadrado = new Cuadrado();
        ps.println("\t Lado del cuadrado: ");
        datoCapturado = scanData.nextDouble();
        cuadrado.setLado(datoCapturado);
        ps.println("\t Area: " + String.valueOf(cuadrado.area()));
        
        cubito = new Cubo(cuadrado.getLado());
        ps.println("\t Su volumen es: " + String.valueOf(cubito.volumen()));
        
        double cubitoAreaRect = ((Cubo) cubito).areaRectangular(cubito.getLado(), cubito.getLado());
        ps.println("\t Su area rectangular es: " + String.valueOf(cubitoAreaRect));
        
        prismaRect = new PrismaRectangular(cuadrado.getLado(), 0, 0);
        ps.println("\t Captura para el prisma Rectangular...");
        ps.println("\t Ancho: ");
        datoCapturado = scanData.nextDouble();
        prismaRect.setAncho(datoCapturado);
        ps.println("\t Alto: ");
        datoCapturado = scanData.nextDouble();
        prismaRect.setAlto(datoCapturado);
        ps.println("\t Su area es: " + String.valueOf(prismaRect.area()));
        ps.println("\t Su volumen es: " + String.valueOf(prismaRect.volumen()));
        
        double prismaAreaRect = ((PrismaRectangular) prismaRect).areaRectangular(prismaRect.getLado(), prismaRect.getAlto());
        ps.println("\t Su area rectangular es: " + String.valueOf(prismaAreaRect));
        
        scanData.close();
    }
}
