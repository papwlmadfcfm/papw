package lmad.papw;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import lmad.papw.figuras.Circulo;
import lmad.papw.figuras.Cuadrado;
import lmad.papw.figuras.Figura;
import lmad.papw.figuras.Triangulo;

public class Main {
	public static void main(String[] args) {
		//1. imprimir en consola
		System.out.println("Hola Mundo!");
		
		//2. solicitar entrada
		Scanner scanner = new Scanner(System.in);		
		System.out.println("Introduce una cadena: ");
		String enterStr1 = scanner.next();
		System.out.println("Capturaste: " + enterStr1);
		System.out.println("Captura un n�mero: ");
		int enterInt1 = scanner.nextInt();
		System.out.println("El n�mero es: " + enterInt1);
		
		//3. if-else
		if (!(enterInt1 % 2 == 0)) {
			System.out.println("el n�mero es RARO e impar");
		} else {
			if (2 <= enterInt1 && enterInt1 <= 5) {
				System.out.println("el n�mero NO ES RARO");
			} else if (6 <= enterInt1 && enterInt1 <= 20) {
				System.out.println("el n�mero es RARO");
			} else if (enterInt1 > 20) {
				System.out.println("el n�mero NO ES RARO pero s� fuera de rango");
			}
		}
		
		//4. M�s entradas y salidas
		System.out.println("Necesito un entero: ");
		int enterInt2 = scanner.nextInt();
		System.out.println("Ahora un double: ");
		double enterDouble1 = scanner.nextDouble();
		float flotante = (float) enterDouble1;
		int entero = (int) flotante;
		System.out.println("Por �ltimo, una cadena: ");
		String enterStr2 = scanner.next();
		System.out.println("Cadena: " + enterStr2);
		System.out.println("Double: " + String.valueOf(enterDouble1));
		System.out.println("Int: " + String.valueOf(enterInt2));
		
		//5. Ciclos
		System.out.println("Ahora, un entero del 1 al 99 para dar sus m�ltiplos");
		int enterInt3 = scanner.nextInt();
		System.out.println("�Cu�ntos m�ltiplos quieres?");
		int enterInt4 = scanner.nextInt();
		int[] multiplos = new int[enterInt4];
		for (int i = 1; i <= enterInt4; i++) {
			multiplos[i-1] = enterInt3*i;
			System.out.println(String.valueOf(enterInt3) + " * " + String.valueOf(i) + " = " + String.valueOf(multiplos[i-1]));
		}
		System.out.print("Enlistados son: ");
		List<Integer> enlistados = new ArrayList<Integer>(multiplos.length);
		for (int multiplo: multiplos) {
			enlistados.add(multiplo);
			System.out.print(String.valueOf(multiplo) + " ");
		}
		enlistados.add(enterInt3 * (enterInt4 + 1));
		System.out.println("El extra es: " + enlistados.get(enlistados.size() - 1));
		System.out.println();
		scanner.close();
		
		// CLASES
		Cuadrado cuadrado = new Cuadrado();
		Circulo circulo = new Circulo();
		Triangulo triangulo = new Triangulo();
		Figura figura = new Figura();
		
		System.out.println("Las instancias creadas son: " + String.valueOf(figura.cont_));
		System.out.println("Las instancias creadas son: " + String.valueOf(cuadrado.cont_));
		System.out.println("Las instancias creadas son: " + String.valueOf(circulo.cont_));
		System.out.println("Las instancias creadas son: " + String.valueOf(triangulo.cont_));

		System.out.println("La variable en bloque est�tico: " + String.valueOf(figura.stat_));
		System.out.println("La variable en bloque est�tico: " + String.valueOf(cuadrado.stat_));
		System.out.println("La variable en bloque est�tico: " + String.valueOf(circulo.stat_));
		System.out.println("La variable en bloque est�tico: " + String.valueOf(triangulo.stat_));

		System.out.println("Y Otro valor es: " + String.valueOf(figura.getOtro()));
		System.out.println("Y Otro valor es: " + String.valueOf(cuadrado.getOtro()));
		System.out.println("Y Otro valor es: " + String.valueOf(circulo.getOtro()));
		System.out.println("Y Otro valor es: " + String.valueOf(triangulo.getOtro()));
	}
}
